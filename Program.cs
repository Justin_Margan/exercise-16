﻿using System;

namespace exercise_16
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            

            Console.WriteLine("*******************************");
            Console.WriteLine("****   Welcome to my app   ****");
            Console.WriteLine("*******************************");
            Console.WriteLine("");
            Console.WriteLine("*******************************");
            Console.WriteLine("What is your name?");
             
            var name = Console.ReadLine();
            
            Console.WriteLine($"Your name is:{name}");
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
